.TH bzfs 6
.SH NAME
bzfs \- bzflag game server
.SH SYNOPSIS
.sS
.B bzfs
[\fB\-a \fIvelocity\fR \fIrotation\fR]
[\fB\-b\fR]
[\fB\-c\fR]
[\fB\+f \fR{\fIgood\fR|\fIflag-id\fR}]
[\fB\-f \fR{\fIbad\fR|\fIflag-id\fR}]
[\fB\-g\fR]
[\fB\-h\fR]
[\fB\-help\fR]
[\fB\-i \fIinterface\fR]
[\fB\-j\fR]
[\fB\-mp \fR{\fIcount\fR|[\fIrogue-count\fR]\fB,\fR[\fIred-count\fR]\fB,\fR[\fIgreen-count\fR]\fB,\fR[\fIblue-count\fR]\fB,\fR[\fIpurple-count\fR]}]
[\fB\-mps \fR{\fImax-score\fR}]
[\fB\-ms \fIshots\fR]
[\fB\-mts \fR{\fImax-score\fR}]
[\fB\-p \fIport\fR]
[\fB\-q\fR]
[\fB\+r\fR]
[\fB\-r\fR]
[\fB\+s \fIflag-count\fR]
[\fB\-s \fIflag-count\fR]
[\fB\-sa\fR]
[\fB\-st \fItime\fR]
[\fB\-sw \fIcount\fR]
[\fB\-synctime\fR]
[\fB\-t\fR]
[\fB\-time \fItime-limit\fR]
[\fB\-ttl \fIttl\fR]
[\fB\-version\fR]
.SH DESCRIPTION
\fBBzfs\fR is the server for \fBbzflag\fR,
and it must be running to play.  It can be run on any system on the
network (including a player's system or one without graphics).
Terminating the server terminates the game in progress.
.SS Options
.RS
.TP 15
\fB-a\fI velocity rotation\fR
Enables inertia and sets the maximum linear and angular accelerations.
The units are somewhat arbitrary so you'll have to experiment to find
suitable values.  The values must be non-negative and higher values
yield greater inertia.
.TP
.B -b
When \fB-c\fR is supplied, this option randomly rotates the buildings.
.TP
.B -c
Enables the capture-the-flag style game.  By default, the free-for-all
style is used.
.TP
\fB+f\fR {\fIgood\fR|\fIflag-id\fR}
Forces the existence of the given flag.  If specified multiple times
for the same \fIflag-id\fR, then that many flags will appear.  The
\fIgood\fR argument is equivalent to specifying \fB+f\fR once for
each kind of good flag.
.TP
\fB-f\fR {\fIbad\fR|\fIflag-id\fR}
Disallows random flags of the given type.  Required flags given by the
\fB+f\fR option are still provided.  The \fIbad\fR argument is equivalent
to specifying \fB-f\fR once for each kind of bad flag.
.TP
.B -g
Quit after serving one game.
.TP
.B -h
Buildings are given random heights.
.TP
.B -help
Shows a help page and lists all the valid flag id's.
.TP
\fB-i\fI interface\fR
Server will listen for and respond to ``pings'' (sent via multicast)
on the given interface.  The server uses the first multicast enabled
interface by default.  Clients use this to find active servers on the
network.
.TP
.B -j
Allows jumping.
.TP
\fB-mp\fR {\fIcount\fR|[\fIrogue\fR]\fB,\fR[\fIred\fR]\fB,\fR[\fIgreen\fR]\fB,\fR[\fIblue\fR]\fB,\fR[\fIpurple\fR]}
Sets the maximum number of players, total or per team.  A single value sets
the total number of players allowed.  Five comma separated values set the
maximum for each team.  If a count is left blank then no limit is set for
that team, except for the limit on the total number of players.  Boths forms
may be provided.
.TP
\fB\-mps\fI max-score\fR
Sets a maximum score for individual players.  The first player to reach
this score is declared the winner and the game is over.
.TP
\fB-ms\fI shots\fR
Allows up to \fIshots\fR simultaneous shots for each player.
This is 1 by default.
.TP
\fB\-mts\fI max-score\fR
Sets a maximum score for teams.  The first team to reach this score is
declared the winner and the game is over.
.TP
\fB-p\fI port\fR
Listen for game connections on \fIport\fR instead of the default port.
Use \fB-help\fR to print the default port.
.TP
.B -q
If specified, the server will not listen for nor respond to ``pings''.
\fBBzflag\fR sends out these pings to give the user a list of
available servers.  This effectively makes the server private,
especially if the \fB-p\fR option is also used.
.TP
.B +r
Makes most shots ricochet.
Super bullets, shock waves, and guided missiles do not.
.TP
.B -r
Allows rogues to join the game.  By default, no rogue players are allowed.
.TP
\fB+s\fI num-flags\fR
The server will have an extra \fInum-flags\fR random super flags available
at all times.  The \fB-f\fR option can be used to restrict which types of
flags will be added.  Required flags given by the \fB+f\fR option are not
included in the \fInum-flags\fR total.
.TP
\fB-s\fI num-flags\fR
The server will have up to \fInum-flags\fR random super flags available at
any time.  The \fB-f\fR option can be used to restrict which types of flags
will be added.  Required flags given by the \fB+f\fR option are not included
in the \fInum-flags\fR total.
.TP
.B -sa
Antidote flags are provided for players with bad flags.
.TP
\fB-st\fI time\fR
Bad flags are automatically dropped after \fItime\fR seconds.
.TP
\fB-sw\fI count\fR
Bad flags are automatically dropped after \fIcount\fR wins.  Capturing
a team flag does not count as a win.
.TP
.B -synctime
Forces all clients to use the same time of day.  The current time is
determined by the server's clock.  This disables the + and - keys on
the clients.
.TP
.B -t
Adds teleporters to the game.
.TP
\fB\-time\fI time-limit\fR
Sets a time limit on the game to \fItime-limit\fR.  The game will be
stopped \fItime-limit\fR seconds after the first player connects.
.TP
\fB-ttl\fI time-to-live\fR
Sets the maximum number of hops a ``ping'' reply will take.  This
effectively limits the range of the server in the network.
Clients more than \fItime-to-live\fR hops away will not receive notification
of the server's existence.
.TP
.B -version
Prints the version number of the executable.
.RE
.SS Notes
The server uses zero CPU time when nobody is playing, and even during
a game the server uses very little CPU, so it's not a burden on the
system to leave one running and it won't interfere with a player
using the same system (except on Windows 95, which \fIreally\fR
sucks at multitasking).  The server will continue to run until
terminated.  If a game is in progress when the server goes down,
all players will be kicked off and the game will be aborted without
warning.  The server resets itself when all players have quit.  All
players must quit to reset the server when a game is over (because of
a score or time limit).
.PP
The following game styles are recommended starting points.
.RS
.TP 15
\fB-c\fR [\fB-b\fR]
Basic capture-the-flag game.  It teaches teamwork and dogfighting skills.
.TP
\fB-h -r -s -t\fR
Free-for-all with superflags and teleporters.  Rogues are allowed.
Teaches players how to use superflags and teleporters for maximum effect.
You may want to allow players to drop bad flags with any of -sa, -st,
and -sw.
.RE
.PP
Notice that the maximum number of shots for these styles is one.  Having
only one shot greatly increases playability and learning speed.  Multiple
shots decrease the required skill level and make it virtually impossible
for even a skilled player to avoid getting shot for any length of time.
More experienced players will still dominate the game, but beginners will
have an easier time making kills.
.SS Networking
Communication between the server and clients (i.e. between \fBbzfs\fR and
\fBbzflag\fR) during a game is via TCP.  Use the \fB-help\fR option to get
the server's default port.  If there's a firewall between the server and
client, the firewall must accept connections from the client to this port
and forward them to the server.
.PP
Clients can search for servers by sending and receiving multicast UDP
packets.  For a client to discover a server, there must be
a multicast route between them with fewer hops than the default TTL
(time-to-live) which is 8 or whatever the TTL was set to using the
\fB-ttl\fR option to \fBbzflag\fR.  However, a client can still connect
to a server beyond the multicast TTL.
.PP
Some communication between clients, such as position and orientation
information, is normally sent directly via multicast UDP packets.  Other
data, like flag grab and kill messages, are sent to the server via TCP
which then turns around and broadcasts it to all players via TCP.  Since
being in a game implies connection to the server, all players are
guaranteed to get all messages sent via TCP.  But the multicast UDP
packets may be routed differently.  If other players can see your tank in
the game but it never appears to move and shots go through it, chances
are high that your multicast packets are not getting routed correctly.
.PP
Bzflag will, upon connecting to a server, attempt to contact the server
via multicast.  If it gets a reply then multicasting is used for the
duration of the game.  If it receives no reply then it falls back to
using the TCP server connection to transmit player-to-player packets.
The server will broadcast these packets to the other players and send
any multicast player-to-player packets to the player via TCP.  This
normally solves any communication problems at the expense of higher
network traffic.  However, there still may be problems if you can
multicast to the server but not to some other players.
.PP
The first thing to check is that multicast packets are being sent on
the right interface.  By default, multicast packets are sent on the
first multicast capable interface.  This is normally the right choice,
but if your system has more than one network interface (other than the
loopback interface), then you may have to use a different interface.
A typical scenario of this situation is a remote system connected via
ISDN to a main network.  Use \fBnetstat -i\fR to list your interfaces,
then use the \fB-interface\fR \fIinterface\fR option to \fBbzflag\fR,
replacing \fIinterface\fR with the correct interface (probably something
like \fBppp0\fR).
.PP
The next thing to check is the TTL (time-to-live) on the client.  By
default it is 8.  This means all other players must be no more than
8 hops (multicast routers) away.  You can use a TTL as high as 255.
Since sites are recommended to not forward multicast packets with TTL's
below 32 outside the site, you cannot normally play bzflag across the
Internet unless you set the TTL above 32.  If any player joins the
game using a TTL greater than everyone else, then the server and all
clients automatically begin using the higher TTL.  This helps ensure
that everyone can see everyone else, but, due the nature of the network
geometry, this cannot be guaranteed.
.PP
If the TTL is high enough, check the multicast network.  You must have
a multicast router between all subnets from your subnet to every other
subnet with a player.  If all players are on the same subnet then no
multicast router is required.  Since multicast routers connecting a
site to the Internet normally prevent packets with TTL's below 32 from
going out onto the Internet, you can't normally play bzflag across the
Internet if the TTL not above 32.  However, you may be able to create
a multicast tunnel, a virtual point-to-point link between any two
subnets on the Internet.  You'll need to run a tunneling router at both
ends of the tunnel.  See \fBmrouted\fR for more information about a
multicast router.
.SH SEE ALSO
bzflag(6), mrouted(1)
